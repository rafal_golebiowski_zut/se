% Rafał Gołębiowski

eliminuj([X], [X]).

eliminuj([X, Y | T], L):-
	eliminuj([Y | T], CurrentL),
	((X \= Y, 
	append([X], CurrentL, L));
	(X == Y,
	append([], CurrentL, L))).

