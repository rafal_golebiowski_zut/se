% Rafał Gołębiowski

eliminuj([X], [X]).
eliminuj([X, X | T], T2):-
	eliminuj([X | T], T2).
eliminuj([X, Y | T], [X | T2]):-
	X \= Y, eliminuj([Y | T], T2).