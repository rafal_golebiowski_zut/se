z5(X, L):-
	append(_, [X | _], L).
	
parzysta([]).
parzysta([_ | T]):-
	nieparzysta(T).

nieparzysta([_ | T]):-
	parzysta(T).
	
odwrotna([], []).
odwrotna([H1 | T1], L2):-
	odwrotna(T1, Tmp),
	append(Tmp, [H1], L2). 
	
	
palindrom(L1):-
	 odwrotna(L1, L1).
	 

shift([H1| T1], L2):-
	append(T1, [H1], L2).
	
sumLista([], 0).
sumLista([H | T], X):-
	sumLista(T, X1),
	X is X1 + H.
	
	
maxLista([X], X).
maxLista([H | T], X):-
	maxLista(T, X1),
	((X1 > H,
	X is X1);
	(H > X1,
	X is H)).
	
	
ostatni([X], X).
ostatni([_ | T], X):-
	ostatni(T, X).
	

przedostatni([X, Y], X).
przedostatni([_ | T], X):-
	przedostatni(T, X).
	
kty(X, [X | T], 0).
kty(X, [X | T], K):-
	K1 is K - 1,
	kty(X, T, K1).
