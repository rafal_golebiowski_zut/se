res(R, R) :-
	number(R).
	
res(serial(R1, R2), W) :-
	res(R1, W1), 
	res(R2, W2),
	W is W1 + W2.

res(parallel(R1, R2), W) :-
	res(R1, W1), 
	res(R2, W2),
	W is (W1 * W2) / (W1 + W2).
	
