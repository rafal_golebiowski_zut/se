zad2(X, M1, M2) :-
	append(_, [M1, X, M2] | _, [sty, lut]).
	
	
zad3(L, X1, X2, X3) :-
	append([X1, X2, X3], _, L).
	
	
zad4(L, X1, X2, X3) :-
	append(_, [X1, X2, X3], L).
	
	
zad5(L, X1) :-
	append(_, [X1 | _], L).
	

parzysta([]).
parzysta([_ | Lista]) :-
	nieparzysta(Lista).
nieparzysta([_ | Lista]) :-
	parzysta(Lista).
	

odwrotna([], []).
odwrotna([L1 | Tail], L2) :-
	odwrotna(Tail, L3),
	append(L3, [L1], L2).
	
palindrom([]).
palindrom(L1) :-
	odwrotna(L1, L1).
	
shift([L1 | X], L2) :-
	append(X, [L1], L2).