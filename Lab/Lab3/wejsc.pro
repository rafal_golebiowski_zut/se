% Rafał Gołębiowski



zad1(X, Y, Z, Z2) :-
	X < Y, Z is Y - X;
	X > Y, Z is X - Y;
	X =:= Y, Z = rowne;
	X < 0, Y < 0, Z2 is X * Y.