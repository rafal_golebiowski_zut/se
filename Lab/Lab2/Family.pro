% Figure 1.8   The family program.


parent( pam, bob).       % Pam is a parent of Bob
parent( tom, bob).
parent( tom, liz).
parent( bob, ann).
parent( bob, pat).
parent( pat, jim).

female( pam).            % Pam is female
female( liz).
female( ann).
female( pat).
male( tom).              % Tom is male
male( bob).
male( jim).

offspring( Y, X)  :-     % Y is an offspring of X if
   parent( X, Y).        % X is a parent of Y

mother( X, Y)  :-        % X is the mother of Y if
   parent( X, Y),        % X is a parent of Y and
   female( X).           % X is female

grandparent( X, Z)  :-   % X is a grandparent of Z if
   parent( X, Y),        % X is a parent of Y and
   parent( Y, Z).        % Y is a parent of Z

sister( X, Y)  :-        % X is a sister of Y if
   parent( Z, X),
   parent( Z, Y),        % X and Y have the same parent and
   female( X).           % X is female and
%   different( X, Y).     % X and Y are different

predecessor( X, Z)  :-   % Rule prl: X is a predecessor of Z
   parent( X, Z).

predecessor( X, Z)  :-   % Rule pr2: X is a predecessor of Z
   parent( X, Y),
   predecessor( Y, Z).

hasAChild(X) :-
	parent(X, _).
	
isGrandparent :-
	grandparent(_, _).

pionowy(odcinek(punkt( X, Y1), punkt( X, Y2))). 
poziomy(odcinek(punkt( X1, Y), punkt( X2, Y))). 

p2(X, Y).
p3(X, Y, Z).

odleglosc(p2(X1,Y1),p2(X2,Y2), R) :-
	R is ((X2-X1)**2 + (Y2-Y1)**2)**0.5.
	
nwd(X, X, X).
nwd(X, Y, D) :- 
	X > Y, R is X-Y, nwd(Y, R, D);
	X < Y, R is Y-X, nwd(X, R, D).