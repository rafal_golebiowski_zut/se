funkcja(X, 0) :-
	X < 3.
funkcja(X, 2) :-
	X >= 3, X < 6.
funkcja(X, 4) :-
	X >= 6.
	
funkcja2(X, 0) :-
	X < 3, !.
funkcja2(X, 2) :-
	X < 6, !.
funkcja2(_, 4).

max(X, Y, Z) :-
	X > Y, !, X == Z;
	Y == Z.
	
	
element([], []).

element([H | T], X) :-
	X == H, !;
	element(T, X).
	

rozne(X, Y) :-
	X \= Y.
	
	
eliminuj([X], [X]).

eliminuj([H, H | T], L) :-
	eliminuj([H | T], L).
	
eliminuj([H, H2 | T], [H | L]) :-
	H \= H2, eliminuj([H2 | T], L).
	
	
