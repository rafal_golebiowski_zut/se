parent(pam, bob).
parent(tom, bob).
parent(tom, liz).
parent(bob, ann).
parent(bob, pat).
parent(pat, jim).

female(pam).
female(liz).
female(ann).
female(pat).
male(jim).
male(tom).
male(bob).

offspring(X, Y) :-
	parent(Y, X).
mother(X, Y) :-
	offspring(Y, X),
	female(X).

father(X, Y) :-
	offspring(Y, X),
	male(X).

grandparent(X, Y) :-
	parent(X, Z), 
	parent(Z, Y).

sister(X, Y) :-
	parent(Z, X),
	parent(Z, Y),
	female(X),
	X \= Y.

predecessor(X, Z) :-
	parent(X, Z).
predecessor(X, Z) :-
	parent(X, Y),
	predecessor(Y, Z).

