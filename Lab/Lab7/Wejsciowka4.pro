% Rafał Gołębiowski

p(a).
p(b) :- !.
p(c).
% 6 ?- p(X), !, p(Y), !.



% testuj - sprawdza czy podany argument nie jest liczba to drugi argument przyjmuje wartosc 'Nie liczba'
% testuj dla liczby przyjmuje wartosc drugiego argumentu 'Liczba'
% testuj(1, 'Liczba').
% number

testuj(X, Y) :-
	\+ number(X), !, Y = 'Nie liczba';
	Y = 'Liczba'.